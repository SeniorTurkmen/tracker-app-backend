import { Router } from "express";
import ogrenciEkle from "../controllers/ogrenci/ogrenci-ekle.js";
import ogrenciGetir from "../controllers/ogrenci/ogrenci-getir.js";
import ogrenciDelivery from "../controllers/ogrenci/ogrenci-set-delivery-status.js";
import ogrenciStatus from "../controllers/ogrenci/ogrenci-set-status.js";


const router = Router();

router.post("/add", ogrenciEkle);
router.post("/edit-history-delivery", ogrenciDelivery);
router.post("/setStatus", ogrenciStatus);
router.get("/get", ogrenciGetir);

export default router;