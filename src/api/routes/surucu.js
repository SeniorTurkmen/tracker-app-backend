import { Router } from "express";
import surucuEkle from "../controllers/surucu/surucu-ekle.js";
import surucugetir from "../controllers/surucu/surucu-getir.js";
import ogrenciAta from "../controllers/surucu/ogrenci_ata.js";


const router = Router();

router.post("/add", surucuEkle);
router.get("/get", surucugetir);
router.post("/ogrenciata", ogrenciAta);

export default router;