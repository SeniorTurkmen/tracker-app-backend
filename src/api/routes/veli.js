import { Router } from "express";
import veliEkle from "../controllers/parent/veli-ekle.js";
import veliGetir from "../controllers/parent/veli-getir.js";
import sifreDegis from "../controllers/parent/parent-sifre-degis.js";


const router = Router();

router.post("/add", veliEkle);
router.get("/get", veliGetir);
router.post("/sifre", sifreDegis);

export default router;