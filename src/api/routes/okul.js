import { Router } from "express";
import okulEkle from "../controllers/okul/okul-ekle.js";
import okulGetir from "../controllers/okul/okul-getir.js";


const router = Router();

router.post("/add", okulEkle);
router.get("/get", okulGetir);

export default router;