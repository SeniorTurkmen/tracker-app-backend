import { Router } from "express";
import swaggerJsdoc from "swagger-jsdoc";
import { serve, setup } from "swagger-ui-express";
import { specs, swaggerConfig } from "../../config/index.js";
import user from "./user.js";
import okul from "./okul.js";
import ogrenci from "./ogrenci.js";
import veli from "./veli.js";
import surucu from "./surucu.js";
import auth from "./auth.js";

const router = Router();

const specDoc = swaggerJsdoc(swaggerConfig);

router.use(specs, serve);
router.get(specs, setup(specDoc, { explorer: true }));

router.use("/user", user);
router.use("/okul", okul);
router.use("/ogrenci", ogrenci);
router.use("/veli", veli);
router.use("/surucu", surucu);
router.use("/auth", auth);

export default router;
