import Joi from "joi";

export function validateOkul(body) {
  const schema = Joi.object({
    email: Joi.string().email().min(3).required(),
    telefon: Joi.string().pattern(/^(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/).required(),
    name: Joi.string().min(3).max(24).required(),
    cadde: Joi.string().min(3).max(24).required(),
    mahalle: Joi.string().min(3).max(24).required(),
    numara: Joi.string().min(3).max(24).required(),
    il: Joi.string().min(3).max(24).required(),
    ilce: Joi.string().min(3).max(24).required(),
    longtidute: Joi.number().min(3).required(),
    latitude: Joi.number().min(3).required(),
    password: Joi.string().min(3).required(),
  });
  return schema.validate(body);
}