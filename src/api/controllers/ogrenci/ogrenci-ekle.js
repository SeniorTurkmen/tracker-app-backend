import {
  errorHelper,
  logger,
  getText,
} from "../../../utils/index.js";
import { Ogrenci, Veli, Adress } from "../../../models/index.js";


export default async (req, res) => {

  let _adress = new Adress({
    cad: req.body.cadde,
    mahalle: req.body.mahalle,
    numara: req.body.numara,
    il: req.body.il,
    ilce: req.body.ilce,
    longtidute: req.body.longtidute,
    latitude: req.body.latitude,
  });
  _adress = await _adress.save().catch((err) => {
    return res.status(500).json(errorHelper("00034", req, err.message));
  });

  let ogrenci;
  try {
    ogrenci = new Ogrenci({
      name: req.body.ogrname,
      surName: req.body.ogrsurName,
      status: req.body.status,
      deliveryStatus: req.body.deliveryStatus,
      telefon: req.body.telefon,
      veli: req.body.id,
      okul: req.body.okul,
      adress: _adress._id,
    });
  } catch (e) {
    console.log(e);
  }

  ogrenci = await ogrenci.save().catch((err) => {
    console.log(err);
    return res.status(500).json(errorHelper("00034", req, err.message));
  });
  let veli = await Veli.findById(req.body.id);
  veli.ogrenciler.push(ogrenci._id);
  veli = await veli.save().catch((err) => {
    console.log(err);
    return res.status(500).json(errorHelper("00034", req, err.message));
  });


  logger("00035", ogrenci._id, getText("en", "00035"), "Info", req);
  return res.status(200).json({
    resultMessage: { en: getText("en", "00035"), tr: getText("tr", "00035") },
    resultCode: "00035",
    veli,
  });
};
