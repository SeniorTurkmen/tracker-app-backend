import { Ogrenci, } from "../../../models/index.js";

export default async (req, res) => {

  try {

    let ogrenci = await Ogrenci.findById(req.body.id).catch((error) => {
      return re.status(400).json({ 'error': "student can't find" });
    });
    var parseddate = new Date();
    var isChanged = false;
    ogrenci.history.forEach(element => {


      if (element.dateTime.getDate() == parseddate.getDate() && element.dateTime.getMonth() == parseddate.getMonth()) {
        element.status.push({ title: req.body.status, time: new Date() })
        ogrenci.status = req.body.status;
        isChanged = true;
      }
    });
    if (!isChanged) {
      ogrenci.history.push(
        {
          dateTime: parseddate,
          deliveryStatus: [
            "Okuldan Eve",
            "Evden Okula"
          ],
          status: [
            {
              title: req.body.status,
              time: new Date()
            }
          ]
        }
      )
      ogrenci.status=req.body.status;
    }

    ogrenci.save().catch((error) => {
      return re.status(400).json({ 'error': "error causing while saving student" });
    })
    return res.status(200).json({
      'success': "proccess complete",
      ogrenci
    });
  } catch (e) {
    print(e)
    return res.status(400).json({
      'success': "proccess complete"
    });
  }
};
