import { Ogrenci } from "../../../models/index.js";

//Bu fonksiyon db den öğrencileri getirmemizi 
//ya da query olarak id yollanmışsa ilgili id 
//ye ait öğrencinin gönderilmesini amaçlamaktadır.
export default async (req, res) => {
  try {
    let ogrenci;
    if (req.query.id == null) {
      ogrenci = await Ogrenci.find({}).populate(
        'adress'
      ).populate('okul').populate('veli');
    } else {
      ogrenci = await Ogrenci.findById(req.query.id).populate(
        'adress'
      ).populate('okul').populate('veli');
    }
    return res.status(200).json(ogrenci);
  } catch (error) {
    return res.status(500).json({ error: err.message });
  }
};