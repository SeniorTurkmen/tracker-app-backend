import {
  errorHelper,
  logger,
  getText,
} from "../../../utils/index.js";
import { Ogrenci, } from "../../../models/index.js";

export default async (req, res) => {

  console.log(req.body);

  let ogrenci = await Ogrenci.findById(req.body.id).catch((error) => {
    return re.status(400).json({ 'error': "student can't find" });
  });
  let parseddate = Date.parse(req.body.date);
  ogrenci.history.push({ dateTime: parseddate, deliveryStatus: req.body.deliveryStatus })

  ogrenci.save().catch((error) => {
    return re.status(400).json({ 'error': "error causing while saving student" });
  })
  return res.status(200).json({
    'success': "proccess complete"
  });
};
