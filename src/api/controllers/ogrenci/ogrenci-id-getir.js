import { Ogrenci } from "../../../models/index.js";

export default async (val) => {
  try {
    let ogrenci = Ogrenci.findById(val);
    
    return ogrenci;
  } catch (error) {
    throw error;
  }
};
