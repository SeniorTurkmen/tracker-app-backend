import { Okul } from "../../../models/index.js";

export default async (_, res) => {
  try {
    const okul = await Okul.find({}).lean();
    okul.forEach((element) => {
      element.password = '***';
    })
    return res.status(200).json(okul);
  } catch (error) {
    return res.status(500).json({ error: err.message });
  }
};