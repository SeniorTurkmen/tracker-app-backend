import { validateOkul } from "../../validators/okul.validator.js";
import {
  errorHelper,
  logger,
  getText,
} from "../../../utils/index.js";
import { Okul, Adress } from "../../../models/index.js";

import bcrypt from "bcryptjs";
const { hash } = bcrypt;

export default async (req, res) => {
  const { error } = validateOkul(req.body);
  if (error) {
    console.log(error);
    return res
      .status(400)
      .json(errorHelper("99999", req, error.details[0].message));
  }
  const exists = await Okul.exists({ email: req.body.email }).catch((err) => {
    return res.status(500).json(errorHelper("00031", req, err.message));
  });

  if (exists) return res.status(409).json(errorHelper("00032", req));


  let _adress = new Adress({
    cad: req.body.cadde,
    mahalle: req.body.mahalle,
    numara: req.body.numara,
    il: req.body.il,
    ilce: req.body.ilce,
    longtidute: req.body.longtidute,
    latitude: req.body.latitude,
  });

  console.log(req.body);

  _adress = await _adress.save().catch((error) => {
    console.log(error);
    return res.status(500).json(errorHelper("99999", req, error.message));
  })
  console.log(_adress);
  const hashed = await hash(req.body.password, 10);


  let okul = new Okul({
    email: req.body.email,
    adress: _adress._id,
    telefon: req.body.telefon,
    name: req.body.name,
    password: hashed
  });

  okul = await okul.save().catch((err) => {
    console.log(err);
    return res.status(500).json(errorHelper("00034", req, err.message));
  });


  logger("00035", okul._id, getText("en", "00035"), "Info", req);
  return res.status(200).json({
    resultMessage: { en: getText("en", "00035"), tr: getText("tr", "00035") },
    resultCode: "00035",
    okul,
  });
};
