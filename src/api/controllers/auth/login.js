import { Okul, Veli, Surucu } from "../../../models/index.js";
import {
  errorHelper,
  getText,
  logger,
} from "../../../utils/index.js";
import bcrypt from "bcryptjs";
const { compare } = bcrypt;

export default async (req, res) => {
  try {
    if (req.body.type == null) {
      return res.status(400).json({ 'error': 'type is required' });
    } else {
      let user;
      if (req.body.email == null) {
        return res.status(400).json({ 'error': 'email is required' });
      } else if (req.body.password == null) {
        return res.status(400).json({ 'error': 'password is required' });

      } else {
        let type = req.body.type.toLowerCase();
        switch (type) {
          case 'okul':
            user = await Okul.findOne({ email: req.body.email })
              .select("+password").populate('adress')
              .catch((err) => {
                return res.status(500).json(errorHelper("00041", req, err.message));
              });
            break;
          case 'surucu':
            user = await Surucu.findOne({ email: req.body.email })
              .select("+password").populate({
                path: 'ogrenciler',
                populate: {
                  path: 'adress',
                }
              }).populate({
                path: 'ogrenciler',
                populate: [{
                  path: 'okul',
                  populate: { path: 'adress' }
                }, {
                  path: 'veli'
                }]
              })
              .catch((err) => {
                return res.status(500).json(errorHelper("00041", req, err.message));
              });
            await user.ogrenciler.forEach(async element => {
              element.okul.password = "***";
              element.veli.password = "***";
            });
            break;
          case 'veli':
            user = await Veli.findOne({ email: req.body.email })
              .select("+password").populate({
                path: 'ogrenciler',
                populate: {
                  path: 'adress',
                }
              })
              .catch((err) => {
                return res.status(500).json(errorHelper("00041", req, err.message));
              });
            console.log(user.ogrenciler);
            user.ogrenciler.forEach(element => {
              element.veli = null;
            });
            break;
        }
        if (!user) return res.status(404).json(errorHelper("00042", req));

        const match = await compare(req.body.password, user.password);
        if (!match) return res.status(400).json(errorHelper("00045", req));

        user.password = "***";

        logger("00047", user._id, getText("en", "00047"), "Info", req);
        return res.status(200).json({
          resultMessage: { en: getText("en", "00047"), tr: getText("tr", "00047") },
          resultCode: "00047",
          user
        });
      }
    }
  } catch (e) {
    console.log(e);
  }
}