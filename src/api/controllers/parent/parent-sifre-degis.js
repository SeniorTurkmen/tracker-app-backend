import {
    errorHelper,
    logger,
    getText,
  } from "../../../utils/index.js";
  import { Veli} from "../../../models/index.js";
  import bcrypt from "bcryptjs";
  const { hash } = bcrypt;
  
  export default async (req, res) => {
    let veli =await Veli.findById(req.body.id)  
    let hashed;  
    try {
      hashed = await hash(req.body.password, 10);
    } catch (e) {
      console.log(e);
    }
    console.log("hashed complete");
  
    veli.password = hashed;
  
    veli = await veli.save().catch((err) => {
      console.log(err);
      return res.status(500).json(errorHelper("00034", req, err.message));
    });
  
    logger("00035", veli._id, getText("en", "00035"), "Info", req);
    return res.status(200).json({
      resultMessage: { en: getText("en", "00035"), tr: getText("tr", "00035") },
      resultCode: "00035",
      veli,
    });
  };
  