import {
  errorHelper,
  logger,
  getText,
} from "../../../utils/index.js";
import { Veli, Ogrenci, Adress } from "../../../models/index.js";
import bcrypt from "bcryptjs";
const { hash, genSalt } = bcrypt;

export default async (req, res) => {


  let _adress = new Adress({
    cad: req.body.cadde,
    mahalle: req.body.mahalle,
    numara: req.body.numara,
    il: req.body.il,
    ilce: req.body.ilce,
    longtidute: req.body.longtidute,
    latitude: req.body.latitude,
  });
  _adress = await _adress.save().catch((err) => {
    console.log(err);
    return res.status(500).json(errorHelper("00034", req, err.message));
  });

  let ogrenci;
  try {
    ogrenci = new Ogrenci({
      name: req.body.name,
      surName: req.body.surName,
      status: req.body.status,
      deliveryStatus: req.body.deliveryStatus,
      telefon: req.body.telefon,
      okul: req.body.okul,
      adress: _adress._id,
    });
  } catch (e) {
    console.log(e);
  }

  ogrenci = await ogrenci.save().catch((err) => {
    return res.status(500).json(errorHelper("00034", req, err.message));
  }); let hashed;
  console.log(req.body);
  try {
    hashed = await hash(req.body.password, 10);
  } catch (e) {
    console.log(e);
  }
  console.log("hashed complete");

  let veli;
  try {
    veli = new Veli({
      name: req.body.name,
      surName: req.body.surName,
      email: req.body.email,
      telefon: req.body.telefon,
      ogrenciler: [ogrenci._id],
      password: hashed
    });
  } catch (e) {
    console.log(e);
  }

  veli = await veli.save().catch((err) => {
    console.log(err);
    return res.status(500).json(errorHelper("00034", req, err.message));
  });

  ogrenci.veli = veli._id;

  ogrenci = await ogrenci.save().catch((err) => {
    return res.status(500).json(errorHelper("00034", req, err.message));
  });

  logger("00035", veli._id, getText("en", "00035"), "Info", req);
  return res.status(200).json({
    resultMessage: { en: getText("en", "00035"), tr: getText("tr", "00035") },
    resultCode: "00035",
    veli,
  });
};
