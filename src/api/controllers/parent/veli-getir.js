import { Veli } from "../../../models/index.js";


export default async (req, res) => {
  try {
    console.log(req.query.id == null);
    let veli;
    if (req.query.id == null) {
      veli = await Veli.find({}).populate({
        path: 'ogrenciler',
        populate: {
          path: 'adress',
        }
      });
      veli.forEach(element => {
        element.password = '***';
      });
    } else {
      veli = await Veli.findById(req.query.id).populate({
        path: 'ogrenciler',
        populate: {
          path: 'adress',
        }
      }).catch((_) => {
        return res.status(400).json({ "error": "not found" });
      });
      if (veli == null) {
        return res.status(400).json({ "error": "not found" });
      }
      veli.password = '***';
    }
    return res.status(200).json(veli);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error: err.message });
  }
};