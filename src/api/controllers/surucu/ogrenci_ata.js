
import {
  errorHelper
} from "../../../utils/index.js";
import { Surucu } from "../../../models/index.js";

export default async (req, res) => {
  try {
    console.log(req.body.id);
    const surucu = await Surucu.findById(req.body.id);
    if (!surucu.ogrenciler.includes(req.body.ogrid))
      surucu.ogrenciler.push(req.body.ogrid)
    else
      return res.status(400).json({ "error": "This student already Attended" });
    await surucu.save().catch((err) => {
      console.log(err);
      return res.status(400).json(errorHelper("00034", req, err.message));
    });
    return res.status(200).json(surucu);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error: err.message });
  }
};