import {
  errorHelper,
  logger,
  getText,
} from "../../../utils/index.js";
import { Surucu } from "../../../models/index.js";
import bcrypt from "bcryptjs";
const { hash } = bcrypt;

export default async (req, res) => {


  console.log(req.body);

  const exists = await Surucu.exists({ email: req.body.email }).catch((err) => {
    return res.status(500).json(errorHelper("00031", req, err.message));
  });

  if (exists) return res.status(409).json(errorHelper("00032", req));
  const hashed = await hash(req.body.password, 10);


  let surucu = new Surucu({
    email: req.body.email,
    telefon: req.body.telefon,
    name: req.body.name,
    surName: req.body.surName,
    password: hashed
  });

  surucu = await surucu.save().catch((err) => {
    console.log(err);
    return res.status(500).json(errorHelper("00034", req, err.message));
  });


  logger("00035", surucu._id, getText("en", "00035"), "Info", req);
  return res.status(200).json({
    resultMessage: { en: getText("en", "00035"), tr: getText("tr", "00035") },
    resultCode: "00035",
    surucu,
  });
};
