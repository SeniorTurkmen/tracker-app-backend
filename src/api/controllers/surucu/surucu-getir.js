import { Surucu } from "../../../models/index.js";

export default async (req, res) => {
  try {
    var surucu;
    if (req.query.id == null) {
      surucu = await Surucu.find({}).populate({
        path: 'ogrenciler',
        populate: {
          path: 'adress',
        }
      });
      surucu.forEach(element => {
        element.password = '***';
      });
    } else {
      surucu = await Surucu.findById(req.query.id).populate({
        path: 'ogrenciler',
        populate: {
          path: 'adress',
        }
      }).catch((_) => {
        return res.status(400).json({ "error": "not found" });
      });
      if (surucu == null) {
        return res.status(400).json({ "error": "not found" });
      };
      surucu.password = '***';
    }

    return res.status(200).json(surucu);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error: err.message });
  }
};