import mongoose from "mongoose";
const { Schema, model } = mongoose;

const adressSchema = new Schema(
  {
    cad: { type: String, },
    mahalle: { type: String, },
    numara: { type: String, },
    il: { type: String, },
    ilce: { type: String, },
    longtidute: { type: Number, },
    latitude: { type: Number, },
  }
);

const Adress = model("Adress", adressSchema);
export default Adress;
