import userModel from "./user.js";
import tokenModel from "./token.js";
import logModel from "./log.js";
import ogrenciModel from "./ogrenci.js";
import surucuModel from "./surucu.js";
import okulModel from "./okul.js";
import veliModel from "./veli.js";
import adressModel from "./adress.js";


export const User = userModel;
export const Token = tokenModel;
export const Okul = okulModel;
export const Ogrenci = ogrenciModel;
export const Surucu = surucuModel;
export const Veli = veliModel;
export const Adress = adressModel;


export const Log = logModel;
