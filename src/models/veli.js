import mongoose from "mongoose";
const { Schema, model } = mongoose;

const veliSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    surName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      lowercase: true,
      match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
    },
    password: {
      type: String,
      required: true,
    },
    telefon: {
      type: String,
      required: true,
      match: /^(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/,
    },
    ogrenciler: [{
      type: Schema.Types.ObjectId, ref: "Ogrenci"
    }],
  },
  {
    timestamps: true,
  }
);

const Veli = model("Veli", veliSchema);
export default Veli;
