import mongoose from "mongoose";
const { Schema, model } = mongoose;

const ogrenciSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    surName: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      required: true,
    },
    okul: {
      type: Schema.Types.ObjectId, ref: "Okul",
    },

    veli: {
      type: Schema.Types.ObjectId, ref: "Veli",
    },
    telefon: {
      type: String,
      required: true,
      match: /^(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/,
    },
    adress: { type: Schema.Types.ObjectId, ref: "Adress" },
    history: [{
      dateTime: {
        type: Date,
      },
      deliveryStatus: [{
        type: String,
      }],
      status: [{
        time: {
          type: Date
        },
        title: {
          type: String
        }
      }]
    }]
  },
  {
    timestamps: true,
  }
);

const Ogrenci = model("Ogrenci", ogrenciSchema);
export default Ogrenci;
